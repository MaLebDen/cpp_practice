# Примеры литералов, определенных пользователем (user defined literals)

* **sea**: [Судоходные вычисления из лекций Алексея Валерьевича Мартынова: числовые литералы](https://gitlab.com/alexeit/cpp_practice/blob/master/user_defined_literals/sea.cpp)
* **cards** [Игральные карты: символьные литералы Unicode](https://gitlab.com/alexeit/cpp_practice/blob/master/user_defined_literals/cards.cpp)